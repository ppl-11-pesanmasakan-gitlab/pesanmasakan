import MenuPopUp from './MenuPopUp';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils'

const dummyMenu = {
    'id' : 1,
    'id_warung' : 1,
    'nama' : 'ayam goreng',
    'harga' : 15000,
    'desc_menu' : 'lalapan ayam goreng dengan nasi, sayur, dan sambal',
    'pic' : '/menu/1-ayam-goreng.jpg'
}

const senin ={
    'hari': 'senin'
}
const jumat ={
    'hari': 'jumat'
}

const dummyDays = [
    jumat,
    senin
]


describe('close modal', () => {
    const element = document.createElement('div');

    const setState = jest.fn();
    const closeFn = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');
    useStateSpy.mockImplementation((init) => [init, setState]);

    beforeEach(() => {
        ReactDOM.render(<MenuPopUp data={dummyMenu} days={dummyDays} open={true} onClose={closeFn} />, element);
    });
    afterEach(() => {
       ReactDOM.unmountComponentAtNode(element);
       setState.mockReset();
    });
    it('when ESC key is pressed', () => {
        var evt = new KeyboardEvent('keydown', { keyCode: 27 });
        document.dispatchEvent(evt);
        expect(setState).toHaveBeenCalledTimes(0);
    });
    it('closes modal if document is clicked', () => {
        const evt = new MouseEvent('click', { bubbles: true });
        document.dispatchEvent(evt);
        expect(setState).toHaveBeenCalledTimes(0);
    });


});