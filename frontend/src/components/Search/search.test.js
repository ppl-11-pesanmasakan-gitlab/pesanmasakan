import { render, fireEvent, act } from '@testing-library/react'
import Searchbar from './Searchbar';
import { createMemoryHistory } from 'history'
import React from 'react'
import { Router } from 'react-router-dom'
import '@testing-library/jest-dom/extend-expect'

describe("Search", () => {
    describe("valid inputs", () => {
        const history = createMemoryHistory({initialEntries: ['/']})
        const route = '/search?location=sipit&query=lahap&menu=ayam'

        it('calls the onSubmit function', async() => {
            
            const {getByPlaceholderText, getByRole} = render(
                <Router history={history}>
                    <Searchbar/>
                </Router>
            )

            await act(async () => {
                fireEvent.change(getByPlaceholderText("Search Name"), {target: {value: "lahap"}})
                fireEvent.change(getByPlaceholderText("Search Location"), {target: {value: "sipit"}})
                fireEvent.change(getByPlaceholderText("Search Menu"), {target: {value: "ayam"}})
            })

            await act(async () => {
                fireEvent.click(getByRole("button"))
                // history.push(route)
            })
            
            // expect(handleSearch).toHaveBeenCalledTimes(1)

            // let location = useLocation();
            expect(history.location.pathname).toBe('/search')
            expect(history.location.search).toBe('?location=sipit&query=lahap&menu=ayam')
        })
    })
})